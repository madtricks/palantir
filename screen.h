#ifndef SCREEN_H
#define SCREEN_H

#include <QLabel>
#include <QPoint>
#include <QRect>

class QMouseEvent;
class QKeyEvent;

class Screen : public QLabel {
  Q_OBJECT
public:
  explicit Screen(QWidget *parent = 0);
signals:
  void clicked(QPoint pos);
  void swiped(QPoint start_pos, QPoint end_pos);
public slots:
  void setPixmap(const QPixmap &);

protected:
  void mousePressEvent(QMouseEvent *ev);
  void mouseReleaseEvent(QMouseEvent *ev);

private:
  static const int swipe_treashold = 10;
  QPoint translatePos(QPoint src);
  QPoint screenToDevice(QPoint screen);
  QRect deviceRect;
  QPoint mousePressPos;
};

#endif // SCREEN_H
