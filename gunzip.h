#ifndef _GUNZIP_H_
#define _GUNZIP_H_

#include <QByteArray>

bool gunzip(const QByteArray in, QByteArray &out);

#endif /*_GUNZIP_H_*/
