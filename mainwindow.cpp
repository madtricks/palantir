#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "adbcommand.h"

#include <QByteArray>
#include <QMenu>
#include <QInputDialog>
#include <QToolButton>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);

  QWidget *spacer = new QWidget(this);
  spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  QToolButton* settings = new QToolButton(this);
  settings->setText("Settings...");
  settings->setPopupMode(QToolButton::InstantPopup);
  settings->setFocusPolicy(Qt::NoFocus);
  QMenu* settingsMenu = new QMenu(settings);

  settingsMenu->addAction(ui->actionADB_Path);
  settings->setMenu(settingsMenu);
  ui->mainToolBar->addWidget(settings);

  ui->mainToolBar->addWidget(spacer);
  ui->mainToolBar->addAction(ui->actionAbout);

  cmd = new AdbCommand("adb",this);
  connect(cmd, SIGNAL(captured(QPixmap)), ui->screen, SLOT(setPixmap(QPixmap)));
  connect(ui->screen, SIGNAL(clicked(QPoint)), cmd, SLOT(tap(QPoint)));
  connect(ui->screen, SIGNAL(swiped(QPoint, QPoint)), cmd,
          SLOT(swipe(QPoint, QPoint)));
  connect(this, SIGNAL(keyPressed(int)), cmd, SLOT(type(int)));
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::keyPressEvent(QKeyEvent *ev) {
  emit keyPressed(ev->key());
  ev->accept();
}

void MainWindow::on_actionConnect_toggled(bool checked)
{
  if (checked) {
    ui->screen->setEnabled(true);
    cmd->startCapture();
  } else {
    ui->screen->setEnabled(false);
    cmd->stopCapture();
  }

}


void MainWindow::on_actionAbout_triggered()
{
   QApplication::aboutQt();
}

void MainWindow::on_actionADB_Path_triggered()
{
    QString path =QInputDialog::getText(this,"Set ADB path","Path:",QLineEdit::Normal,cmd->getAdbPath());
    if (!path.isNull())
        cmd->setAdbPath(path);
}
