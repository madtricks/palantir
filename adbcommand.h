#ifndef ADBCOMMAND_H
#define ADBCOMMAND_H

#include <QObject>
#include <QPixmap>
#include <QPoint>
#include <QKeyEvent>
#include <QString>

class QProcess;

class AdbCommand : public QObject {
  Q_OBJECT
public:
  explicit AdbCommand(QString adb_path, QObject *parent = 0);
  int getOrientation();
  QString getAdbPath();
  void setAdbPath(QString path);
  ~AdbCommand();
signals:
  void captured(QPixmap pix);

public slots:
  void tap(QPoint position);
  void swipe(QPoint pos1, QPoint pos2);
  void type(int key);
  void startCapture(bool repeating = true);
  void stopCapture();
private slots:
  void processScreencap(int);

private:
  static QMap<int, QString> keymap;
  QProcess *screencap;
  bool repeating;
  QString adb_path;
};

#endif // ADBCOMMAND_H
