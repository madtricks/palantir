#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>

class QTimer;
class AdbCommand;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

protected:
  void keyPressEvent(QKeyEvent *ev);

private slots:
  void on_actionConnect_toggled(bool checked);

  void on_actionAbout_triggered();

  void on_actionADB_Path_triggered();

signals:
  void keyPressed(int key);

private:
  Ui::MainWindow *ui;
  AdbCommand *cmd;
};

#endif // MAINWINDOW_H
