#-------------------------------------------------
#
# Project created by QtCreator 2014-11-29T19:20:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = palantir
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    screen.cpp \
    adbcommand.cpp \
    gunzip.cpp

HEADERS  += mainwindow.h \
    screen.h \
    adbcommand.h \
    gunzip.h

FORMS    += mainwindow.ui

CONFIG += c++11
LIBS += -lz
