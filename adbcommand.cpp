#include "adbcommand.h"
#include <QProcess>
#include <QTransform>
#include <QDebug>
#include <QStringList>
#include <QCryptographicHash>
#include <QFile>

#include "gunzip.h"

QMap<int, QString> AdbCommand::keymap = {
    std::make_pair(Qt::Key_0, "KEYCODE_0"),
    std::make_pair(Qt::Key_1, "KEYCODE_1"),
    std::make_pair(Qt::Key_2, "KEYCODE_2"),
    std::make_pair(Qt::Key_3, "KEYCODE_3"),
    std::make_pair(Qt::Key_4, "KEYCODE_4"),
    std::make_pair(Qt::Key_5, "KEYCODE_5"),
    std::make_pair(Qt::Key_6, "KEYCODE_6"),
    std::make_pair(Qt::Key_7, "KEYCODE_7"),
    std::make_pair(Qt::Key_8, "KEYCODE_8"),
    std::make_pair(Qt::Key_9, "KEYCODE_9"),
    std::make_pair(Qt::Key_A, "KEYCODE_A"),
    std::make_pair(Qt::Key_B, "KEYCODE_B"),
    std::make_pair(Qt::Key_C, "KEYCODE_C"),
    std::make_pair(Qt::Key_D, "KEYCODE_D"),
    std::make_pair(Qt::Key_E, "KEYCODE_E"),
    std::make_pair(Qt::Key_F, "KEYCODE_F"),
    std::make_pair(Qt::Key_G, "KEYCODE_G"),
    std::make_pair(Qt::Key_H, "KEYCODE_H"),
    std::make_pair(Qt::Key_I, "KEYCODE_I"),
    std::make_pair(Qt::Key_J, "KEYCODE_J"),
    std::make_pair(Qt::Key_K, "KEYCODE_K"),
    std::make_pair(Qt::Key_L, "KEYCODE_L"),
    std::make_pair(Qt::Key_M, "KEYCODE_M"),
    std::make_pair(Qt::Key_N, "KEYCODE_N"),
    std::make_pair(Qt::Key_O, "KEYCODE_O"),
    std::make_pair(Qt::Key_P, "KEYCODE_P"),
    std::make_pair(Qt::Key_Q, "KEYCODE_Q"),
    std::make_pair(Qt::Key_R, "KEYCODE_R"),
    std::make_pair(Qt::Key_S, "KEYCODE_S"),
    std::make_pair(Qt::Key_T, "KEYCODE_T"),
    std::make_pair(Qt::Key_U, "KEYCODE_U"),
    std::make_pair(Qt::Key_V, "KEYCODE_V"),
    std::make_pair(Qt::Key_W, "KEYCODE_W"),
    std::make_pair(Qt::Key_X, "KEYCODE_X"),
    std::make_pair(Qt::Key_Y, "KEYCODE_Y"),
    std::make_pair(Qt::Key_Z, "KEYCODE_Z"),
    std::make_pair(Qt::Key_Backspace, "KEYCODE_DEL"),
    std::make_pair(Qt::Key_Space, "KEYCODE_SPACE"),
};

AdbCommand::AdbCommand(QString adb_path, QObject *parent) : QObject(parent), adb_path(adb_path) {
  screencap = new QProcess(this);
  connect(screencap, SIGNAL(finished(int)), this, SLOT(processScreencap(int)));
}

int AdbCommand::getOrientation() {
  QProcess p;
  QStringList args;
  args << "shell"
       << "dumpsys input | grep SurfaceOrientation | grep -o -e [[:digit:]]";
  p.start("adb", args);
  p.waitForFinished(-1);
  auto data = p.readAllStandardOutput();
  data.chop(2);
  int orientation = data.toInt();
  switch (orientation) {
  case 1:
    return -90;
  case 3:
    return 90;
  default:
    return 0;
  }
}

QString AdbCommand::getAdbPath()
{
    return adb_path;
}

void AdbCommand::setAdbPath(QString path)
{
    adb_path = path;
}

AdbCommand::~AdbCommand() { screencap->close(); }

void AdbCommand::tap(QPoint position) {
  QProcess::execute(
      QString("adb shell input tap %1 %2").arg(position.x()).arg(position.y()));
}

void AdbCommand::swipe(QPoint pos1, QPoint pos2) {
  QProcess::execute(QString("adb shell input swipe %1 %2 %3 %4")
                        .arg(pos1.x())
                        .arg(pos1.y())
                        .arg(pos2.x())
                        .arg(pos2.y()));
}

void AdbCommand::type(int keycode) {
  auto it = keymap.find(keycode);
  if (it != keymap.end()) {
    QProcess::execute(QString("adb shell input keyevent %1").arg(it.value()));
  }
}

void AdbCommand::startCapture(bool repeating) {
  this->repeating = repeating;
  QStringList args;
  args << "shell"
       << "screencap -p | gzip";
  screencap->start("adb", args);
}

void AdbCommand::stopCapture() { repeating = false; }

void AdbCommand::processScreencap(int) {

  QByteArray gzipped = screencap->readAllStandardOutput().replace("\r\n", "\n");
  QByteArray data;
  gunzip(gzipped, data);
  QPixmap pixmap;
  pixmap.loadFromData(data, "PNG");
  if (!pixmap.isNull()) {
    pixmap = pixmap.transformed(QTransform().rotate(getOrientation()));
    emit captured(pixmap);
  }
  if (repeating)
    startCapture();
}
