#include "screen.h"
#include <QMouseEvent>

#include <QDebug>

Screen::Screen(QWidget *parent) : QLabel(parent) { setEnabled(false); }

void Screen::setPixmap(const QPixmap &p) {
  deviceRect = QRect(QPoint(0, 0), p.size());
  QPixmap scaled =
      p.scaled(size().boundedTo(deviceRect.size()), Qt::KeepAspectRatio);
  QLabel::setPixmap(scaled);
}

void Screen::mousePressEvent(QMouseEvent *ev) { mousePressPos = ev->pos(); }

void Screen::mouseReleaseEvent(QMouseEvent *ev) {
  if (!pixmap() || pixmap()->isNull())
    return;

  QPoint startingPoint = screenToDevice(translatePos(mousePressPos));
  QPoint endingPoint = screenToDevice(translatePos(ev->pos()));

  if (deviceRect.contains(startingPoint) && deviceRect.contains(endingPoint)) {
    int distance = (endingPoint - startingPoint).manhattanLength();
    if (distance < swipe_treashold)
      emit clicked(endingPoint);
    else
      emit swiped(startingPoint, endingPoint);
  }
}

QPoint Screen::translatePos(QPoint src) {
  QPoint origin;
  origin.setX(width() / 2 - pixmap()->width() / 2);
  origin.setY(height() / 2 - pixmap()->height() / 2);
  return src - origin;
}

QPoint Screen::screenToDevice(QPoint screen) {
  double xscale = static_cast<double>(deviceRect.width()) /
                  static_cast<double>(pixmap()->width());
  double yscale = static_cast<double>(deviceRect.height()) /
                  static_cast<double>(pixmap()->height());
  return QPoint(screen.x() * xscale, screen.y() * yscale);
}
